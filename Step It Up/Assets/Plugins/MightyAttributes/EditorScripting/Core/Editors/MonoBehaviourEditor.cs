#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace MightyAttributes.Editor
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(MonoBehaviour), true)]
    internal class MonoBehaviourEditor : BaseMightyEditor
    {
        private void OnSceneGUI()
        {
            if (MightySettingsServices.Activated) SceneGUI();
        }
    }
}
#endif