﻿#if UNITY_EDITOR
namespace MightyAttributes.Editor
{
    public interface ISceneDrawer : ISpecialDrawer
    {
        void OnGUI(MightySerializedField serializedField, BaseSceneAttribute baseAttribute);
    }

    public abstract class BaseSceneDrawer<T> : BaseSpecialDrawer<T>, ISceneDrawer where T : BaseSceneAttribute
    {
        public void OnGUI(MightySerializedField serializedField, BaseSceneAttribute baseAttribute) => 
            OnGUI(serializedField, (T) baseAttribute);

        protected abstract void OnGUI(MightySerializedField serializedField, T attribute);
    }
}
#endif
