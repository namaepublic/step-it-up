﻿using EncryptionTool;
using MightyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    [SerializeField, FindAssets, Reorderable(false, options: ArrayOption.DisableSizeField)]
    private LevelModel[] _levels;

    [SerializeField, FindObject] private GUIManager _guiManager;

    [SerializeField, FindObject] private PlayerBehaviour _playerBehaviour;

    [SerializeField, FindObject] private EncryptionInitializer _encryptionInitializer;

    private bool m_playing;

    public PlayerBehaviour PlayerBehaviour => _playerBehaviour;

    private void Start() => Init();

    public void Init()
    {
        Instance = this;

        m_playing = false;

        SavedDataServices.Init(_encryptionInitializer);
        SavedDataServices.LoadEverythingFromLocal();
        
        LoadLevel(SavedDataServices.LevelIndex);
    }

    private void ResetButtons()
    {
        _guiManager.DisplayNextButton(false);
        _guiManager.EnableNextButton(false);
        _guiManager.DisplayStartButton(true);
        _guiManager.EnableStartButton(false);
    }

    public void InitBehaviours(Vector3 startPosition, Quaternion startRotation) => _playerBehaviour.Init(startPosition, startRotation);

    public void WaitForPlay()
    {
        m_playing = false;
        _guiManager.EnableStartButton(true);
    }

    public void Play()
    {
        m_playing = true;
        _guiManager.DisplayStartButton(false);
        _playerBehaviour.Play();
    }

    public void WaitForNext()
    {
        m_playing = false;
        _guiManager.DisplayNextButton(true);
        _guiManager.EnableStartButton(true);
    }

    private void Update()
    {
        if (m_playing)
            _playerBehaviour.UpdatePlayer();
    }

    private void FixedUpdate()
    {
        if (m_playing)
            _playerBehaviour.FixedUpdatePlayer();
    }

    public void LoadNextLevel()
    {
        var index = SavedDataServices.LevelIndex;

        if (IsLevelLoaded(index)) UnloadLevel(index);
        LoadLevel(++index);
    }

    public void LoadLevel(byte index, bool saveProgression = true)
    {
        ResetButtons();
     
        if (index >= _levels.Length) index = 0;

        if (IsLevelLoaded(index)) UnloadLevel(index);
        if (saveProgression) SavedDataServices.LevelIndex = index;

        SceneManager.LoadScene(_levels[index].SceneIndex, LoadSceneMode.Additive);
    }

    public void UnloadLevel(byte index) => SceneManager.UnloadSceneAsync(_levels[index].SceneIndex);

    public bool IsLevelLoaded(byte index) => _levels[index].IsLoaded();

    public void ReloadLevel() => LoadLevel(SavedDataServices.LevelIndex, false);
}