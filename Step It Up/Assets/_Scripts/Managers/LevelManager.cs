﻿using MightyAttributes;
#if UNITY_EDITOR
using MightyAttributes.Utilities;
#endif
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }

    [SerializeField, FindObject, ReadOnly] private GoalBehaviour _goal;

    [SerializeField, PositionHandle] private Vector3 _startPosition;
    [SerializeField, PositionHandle] private Vector3 _startRotation;
    [SerializeField] private float _minHeight;
    [SerializeField] private Transform _placedBoardsTransform;
    
    public float MinHeight => _minHeight;

    public Transform PlacedBoardsTransform => _placedBoardsTransform;

    private void Start() => Init();

    public void Init()
    {
        Instance = this;
        _goal.Init();
        GameManager.Instance.InitBehaviours(_startPosition, Quaternion.Euler(_startRotation));

        GameManager.Instance.WaitForPlay();
    }

    public void EndLevel() => GameManager.Instance.WaitForNext();

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        var player = ReferencesUtilities.FindFirstObject<PlayerBehaviour>();
        if (!player) return;

        var playerPosition = player.transform.position;
        var size = 100;

        GizmosUtilities.DrawTopDownRectangle(new Vector3(playerPosition.x, _minHeight, playerPosition.z), Quaternion.identity,
            new Vector2(size, size), new Color(1, 1, 0, .05f), new Color(1, 1, 0));
    }
#endif
}