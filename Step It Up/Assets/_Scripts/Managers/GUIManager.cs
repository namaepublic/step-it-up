﻿using UnityEngine;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour
{
    [SerializeField] private Button _startButton;
    [SerializeField] private Button _nextButton;

    public void DisplayStartButton(bool display) => _startButton.gameObject.SetActive(display);
    public void DisplayNextButton(bool display) => _nextButton.gameObject.SetActive(display);

    public void EnableStartButton(bool enabled) => _startButton.interactable = enabled;
    public void EnableNextButton(bool enabled) => _startButton.interactable = enabled;
}
