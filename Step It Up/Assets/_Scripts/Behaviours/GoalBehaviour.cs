﻿using MightyAttributes;
using UnityEngine;

public class GoalBehaviour : MonoBehaviour
{
    [SerializeField, AssetOnly] private ObstacleBehaviour _goalObstaclePrefab;
    [SerializeField, AssetOnly] private GoalBlockBehaviour _blockPrefab;

    [SerializeField] private bool _addObstacle = true;
    [SerializeField] private byte _blockCount;
    [SerializeField] private Gradient _blockGradient;
    [SerializeField, ReadOnly] private GoalBlockBehaviour[] _goalBlocks;

    [SerializeField, LayerField] private int _playerLayer;

    private bool m_finished;

    public void Init()
    {
        m_finished = false;
        
        for (var i = 0; i < _blockCount; i++)
            _goalBlocks[i].SetColor(_blockGradient.Evaluate((float) i / _blockCount));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != _playerLayer) return;

        GameManager.Instance.PlayerBehaviour.ClimbingGoal = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (m_finished) return;
        
        if (other.gameObject.layer != _playerLayer) return;

        var player = GameManager.Instance.PlayerBehaviour;
        var destination = _goalBlocks[_blockCount - 1].GetDestination();
        if (player.GetPosition().y >= destination.y)
        {
            player.FinishLevel(destination);
            m_finished = true;
        }
        else
            player.IncreaseClimbSpeed();
    }

#if UNITY_EDITOR
    [Order(999), Button]
    private void PopulateBlocks()
    {
        var parent = transform;

        while (parent.childCount > 0)
            DestroyImmediate(parent.GetChild(0).gameObject);

        _goalBlocks = new GoalBlockBehaviour[_blockCount];
        
        if (_addObstacle)
            Instantiate(_goalObstaclePrefab, parent);

        if (_blockCount <= 0) return;
        
        for (byte i = 0; i < _blockCount; i++)
        {
            var block = Instantiate(_blockPrefab, parent);
            block.Init(i);

            _goalBlocks[i] = block;
        }

        var collider = GetComponent<BoxCollider>();
        if (!collider) return;

        collider.size = new Vector3(collider.size.x, _blockCount + .5f, _blockCount + 1.5f);
        collider.center = new Vector3(0, (float) _blockCount / 2 + .25f, (float) _blockCount / 2 - 1.25f);
    }
#endif
}