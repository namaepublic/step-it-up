﻿using System.Collections.Generic;
using UnityEngine;

public class BoardStackBehaviour : MonoBehaviour
{
    [SerializeField] private Vector3 _storeBoardSize;
    [SerializeField] private short _columnAmount;

    private readonly Stack<BoardBehaviour> m_boards = new Stack<BoardBehaviour>();

    private short m_columnCount;
    private short m_lineCount;

    public ushort Count => (ushort) m_boards.Count;

    public bool IsEmpty() => Count == 0;

    public void AddBoard(BoardBehaviour board)
    {
        var tfm = transform;

        board.MoveTo(tfm, new Vector3(0, _storeBoardSize.y * m_lineCount, m_columnCount * -_storeBoardSize.z), tfm.rotation,
            _storeBoardSize);
        m_boards.Push(board);

        UpdateCount(true);
    }

    public bool TryRemoveBoard(out BoardBehaviour board)
    {
        if (!IsEmpty())
        {
            board = m_boards.Pop();
            UpdateCount(false);
            return true;
        }

        board = null;
        return false;
    }

    public void Clear()
    {
        while (!IsEmpty())
            Destroy(m_boards.Pop().gameObject);

        m_columnCount = m_lineCount = 0;
    }

    public void DropAmount(short lostBoardAmound)
    {
        short count = 0;
        while (!IsEmpty() && count++ < lostBoardAmound)
        {
            var board = m_boards.Pop();
            var tfm = board.transform;
            board.MoveTo(LevelManager.Instance.PlacedBoardsTransform, tfm.position, tfm.rotation, _storeBoardSize);
            board.DropImmediately();
            UpdateCount(false);
        }
    }

    private void UpdateCount(bool increase)
    {
        if (increase)
        {
            if (++m_columnCount < _columnAmount) return;

            m_lineCount++;
            m_columnCount = 0;
        }
        else
        {
            if (--m_columnCount >= 0) return;

            m_lineCount--;
            m_columnCount = (short) (_columnAmount - 1);
        }
    }
}