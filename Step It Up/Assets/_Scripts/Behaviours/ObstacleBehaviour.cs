﻿using MightyAttributes;
using UnityEngine;

public class ObstacleBehaviour : MonoBehaviour
{
    [SerializeField, LayerField] private int _playerLayer;
    
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer != _playerLayer) return;
        
        GameManager.Instance.PlayerBehaviour.Push();
    }
}
