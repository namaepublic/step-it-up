﻿using DG.Tweening;
using MightyAttributes;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    private enum PlayerState : byte
    {
        None,

        Idle,
        Running,
        Falling,
        Climbing,
        Pushed,
        Finishing
    }

    private enum AnimatorState : byte
    {
        Idle,
        Running,
        Falling
    }

    // @formatter:off

    [Title("Movements")]
    [SerializeField, GetComponent, ReadOnly] private Rigidbody _rigidbody;
    [SerializeField] private float _speed;
    [SerializeField] private float _goalSpeed;

    [Title("Raycast")] 
    [SerializeField] private Vector3 _rayOffset;
    [SerializeField] private float _rayLength;
    [SerializeField, LayerField] private int _trackLayer;
    [SerializeField, LayerField] private int _goalBlockLayer;
    
    [Title("Climb")]
    [SerializeField, GetComponentInChildren, ReadOnly] private BoardStackBehaviour _boardStack;
    [SerializeField] private Vector3 _placedBoardSize;
    [SerializeField] private Vector3 _placedBoardRotation;
    [SerializeField] private float _climbOffset;
    [SerializeField] private float _climbSpeedIncrement;
    
    [Title("Obstacle Hit")]
    [SerializeField] private Vector3 _pushOffset;
    [SerializeField] private short _lostBoardAmount;
    
    [Title("Animation")]
    [SerializeField, GetComponentInChildren, ReadOnly] private Animator _animator;
    [SerializeField, AnimatorParameter("Idle"), ReadOnly] private int _idleID;
    [SerializeField, AnimatorParameter("Running"), ReadOnly] private int _runningID;
    [SerializeField, AnimatorParameter("Falling"), ReadOnly] private int _fallingID;
    
    // @formatter:on

    private Tweener m_climbTweener;
    private Tweener m_pushedTweener;

    private TweenCallback m_onTurnComplete;
    private TweenCallback m_onPushComplete;
    private TweenCallback m_onFinishingComplete;

    private LayerMask m_trackMask;

    private float m_speed;
    private float m_speedIncrement;

    private float m_climbDuration;
    private float m_obstacleDuration;

    private Vector3 m_turnAngles;
    private Vector3 m_turnDestination;

    private Vector3 m_finishPosition;

    [Title("Non Serialized")]
    [ShowNonSerialized] private PlayerState m_playerState;
    [ShowNonSerialized] private AnimatorState m_animatorState;

    private bool m_tryingToClimb;

    private bool m_init;

    public bool ClimbingGoal { get; set; }
    
    public Vector3 GetPosition() => _rigidbody.position;

    public void Init(Vector3 position, Quaternion rotation)
    {
        if (!m_init)
        {
            m_climbTweener = m_pushedTweener = _rigidbody.DOMove(Vector3.zero, 0);

            m_onTurnComplete = OnTurnComplete;
            m_onPushComplete = OnPushComplete;
            m_onFinishingComplete = OnFinishingComplete;

            m_climbDuration = _climbOffset / _speed;
            m_obstacleDuration = _pushOffset.magnitude / _speed;

            m_trackMask = 1 << _trackLayer | 1 << _goalBlockLayer;
        }

        Stop();

        var tfm = transform;
        tfm.position = position;
        tfm.rotation = rotation;

        _rigidbody.position = position;

        _boardStack.Clear();

        m_init = true;
    }

    public void Play()
    {
        SetPlayerState(PlayerState.Running);
    }

    public void Stop()
    {
        _rigidbody.velocity = Vector3.zero;

        _rigidbody.DOComplete();

        m_tryingToClimb = false;
        m_speedIncrement = 0;
        ClimbingGoal = false;

        SetPlayerState(PlayerState.Idle);
    }

    public void TurnTowards(Vector3 angles, Vector3 destination)
    {
        m_turnAngles = angles;
        m_turnDestination = destination;
        
        DoTurn();
    }

    public void AddBoard(BoardBehaviour board) => _boardStack.AddBoard(board);

    public void Push()
    {
        if (m_pushedTweener.active) return;

        if (_boardStack.IsEmpty())
        {
            Stop();
            GameManager.Instance.ReloadLevel();
            return;
        }

        _rigidbody.DOComplete();

        SetPlayerState(PlayerState.Pushed);

        _boardStack.DropAmount(_lostBoardAmount);
    }

    public void IncreaseClimbSpeed() => m_speedIncrement += _climbSpeedIncrement;

    public void FinishLevel(Vector3 position)
    {
        if (m_playerState == PlayerState.Idle || m_playerState == PlayerState.Finishing) return;

        m_finishPosition = position;
        SetPlayerState(PlayerState.Finishing);
    }

    private void SetPlayerState(PlayerState state)
    {
        m_playerState = state;
        switch (state)
        {
            case PlayerState.Idle:
                _rigidbody.useGravity = true;
                SetAnimatorState(AnimatorState.Idle);
                break;
            case PlayerState.Running:
                m_speed = _speed; 
                SetAnimatorState(AnimatorState.Running);
                break;
            case PlayerState.Climbing:
                _rigidbody.useGravity = false;
                DoClimb();
                SetAnimatorState(AnimatorState.Running);
                break;
            case PlayerState.Pushed:
                _rigidbody.useGravity = false;
                DoPush();
                SetAnimatorState(AnimatorState.Falling);
                break;
            case PlayerState.Falling:
                SetAnimatorState(AnimatorState.Falling);
                break;
            case PlayerState.Finishing:
                _rigidbody.useGravity = false;
                DoFinish();
                SetAnimatorState(AnimatorState.Falling);
                break;
            case PlayerState.None:
                _rigidbody.useGravity = true;
                break;
        }
    }

    private void DoClimb()
    {
        var velocity = _rigidbody.velocity;
        _rigidbody.velocity = new Vector3(velocity.x, 0, velocity.z);
        
        m_climbTweener = _rigidbody.DOMoveY(_climbOffset * (m_speed + m_speedIncrement), m_climbDuration).SetEase(Ease.Linear).SetRelative()
            .SetUpdate(UpdateType.Fixed);
    }

    private void DoTurn()
    {
        var duration = (m_turnDestination - GetPosition()).magnitude / _speed;
        _rigidbody.DORotate(m_turnAngles, duration).SetEase(Ease.Linear).SetUpdate(UpdateType.Fixed).OnComplete(m_onTurnComplete);
    }

    private void DoPush()
    {
        m_pushedTweener = _rigidbody.DOMove(transform.TransformDirection(_pushOffset), m_obstacleDuration).SetEase(Ease.OutSine)
            .SetRelative().SetUpdate(UpdateType.Fixed).OnComplete(m_onPushComplete);
    }

    private void DoFinish()
    {
        _rigidbody.DOMove(m_finishPosition, (m_finishPosition - GetPosition()).magnitude / (_speed * m_speed))
            .SetEase(Ease.Linear).SetUpdate(UpdateType.Fixed).OnComplete(m_onFinishingComplete);
    }

    private void SetAnimatorState(AnimatorState state)
    {
        if (m_animatorState == state) return;

        m_animatorState = state;
        switch (state)
        {
            case AnimatorState.Idle:
                _animator.ResetTrigger(_runningID);
                _animator.ResetTrigger(_fallingID);
                _animator.SetTrigger(_idleID);
                break;
            case AnimatorState.Running:
                _animator.ResetTrigger(_idleID);
                _animator.ResetTrigger(_fallingID);
                _animator.SetTrigger(_runningID);
                break;
            case AnimatorState.Falling:
                _animator.ResetTrigger(_idleID);
                _animator.ResetTrigger(_runningID);
                _animator.SetTrigger(_fallingID);
                break;
        }
    }

    public void UpdatePlayer()
    {
        if (m_playerState == PlayerState.Idle) return;

        if (Input.GetMouseButtonDown(0) && !_boardStack.IsEmpty())
            m_tryingToClimb = true;
        else if (Input.GetMouseButtonUp(0))
            m_tryingToClimb = false;
    }

    public void FixedUpdatePlayer()
    {
        var position = GetPosition();

        if (m_playerState == PlayerState.Idle || m_playerState == PlayerState.Finishing) return;

        if (position.y < LevelManager.Instance.MinHeight)
        {
            Stop();
            GameManager.Instance.ReloadLevel();
            return;
        }

        if (m_playerState == PlayerState.Pushed) return;

        m_speed = ClimbingGoal ? _goalSpeed : _speed;
        
        if (m_tryingToClimb)
            TryClimb(position);
        else if (m_playerState == PlayerState.Climbing)
            SetPlayerState(PlayerState.None);

        var direction = (m_speed + m_speedIncrement) * transform.forward;
        _rigidbody.velocity = new Vector3(direction.x, _rigidbody.velocity.y, direction.z);

        if (m_playerState == PlayerState.Climbing) return;

        position = GetPosition();

        if (Physics.Raycast(GetTrackRay(position), out var hit, _rayLength, m_trackMask))
        {
            var tfm = hit.transform;
            var layer = tfm.gameObject.layer;

            if (layer == _goalBlockLayer)
            {
                FinishLevel(tfm.parent.GetComponent<GoalBlockBehaviour>().GetDestination());
                return;
            }

            if (layer == _trackLayer)
                SetPlayerState(PlayerState.Running);
        }
        else SetPlayerState(PlayerState.Falling);
    }

    private void TryClimb(Vector3 position)
    {
        if (m_climbTweener.active) return;
        if (_boardStack.TryRemoveBoard(out var board))
        {
            SetPlayerState(PlayerState.Climbing);

            board.MoveTo(LevelManager.Instance.PlacedBoardsTransform, position,
                Quaternion.Euler(transform.rotation.eulerAngles + _placedBoardRotation), _placedBoardSize);
            board.Drop();

            return;
        }

        SetPlayerState(PlayerState.None);
        m_tryingToClimb = false;
    }

    private Ray GetTrackRay(Vector3 position) => new Ray(position + _rayOffset, Vector3.down);


    private void OnTurnComplete()
    {
        transform.rotation = Quaternion.Euler(m_turnAngles);
        _rigidbody.position = new Vector3(m_turnDestination.x, GetPosition().y, m_turnDestination.z);
    }
    
    private void OnPushComplete() => SetPlayerState(PlayerState.None);

    private void OnFinishingComplete()
    {
        if (m_playerState != PlayerState.Finishing) return;
        
        _rigidbody.position = m_finishPosition;
        Stop();
        LevelManager.Instance.EndLevel();
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        var ray = GetTrackRay(transform.position);
        Debug.DrawRay(ray.origin, ray.direction * _rayLength, Color.red);
    }
#endif
}