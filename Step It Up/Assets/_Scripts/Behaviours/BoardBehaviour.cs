﻿using System.Collections;
using MightyAttributes;
using UnityEngine;

public class BoardBehaviour : MonoBehaviour
{
    private static readonly WaitForSeconds m_dropWFS = new WaitForSeconds(.15f); 
    
    [SerializeField] private Collider _collider;

    [SerializeField, LayerField] private int _playerLayer;

    [SerializeField] private Rigidbody _graphicsRigidbody;
    [SerializeField] private Collider _graphicsCollider;
    [SerializeField] private Transform _graphicsTransform;

    private bool m_collected;

    public void MoveTo(Transform parent, Vector3 position, Quaternion rotation, Vector3 size)
    {
        var tfm = transform;
        tfm.parent = parent;
        tfm.localPosition = position;
        tfm.rotation = rotation;
        _graphicsTransform.localScale = size;
    }

    public void Drop() => StartCoroutine(DropCoroutine());

    private IEnumerator DropCoroutine()
    {
        yield return m_dropWFS;
        DropImmediately();
    }

    public void DropImmediately()
    {
        _graphicsRigidbody.isKinematic = false;
        _graphicsCollider.enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (m_collected) return;

        if (other.gameObject.layer != _playerLayer) return;
        
        GameManager.Instance.PlayerBehaviour.AddBoard(this);
        _collider.enabled = false;
        m_collected = true;
    }
}