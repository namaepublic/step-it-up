﻿using MightyAttributes.Utilities;
using TMPro;
using UnityEngine;

public class GoalBlockBehaviour : MonoBehaviour
{
    private static readonly Vector3 Offset = new Vector3(0, 1, 1);

    [SerializeField] private MeshRenderer _renderer;
    [SerializeField] private TMP_Text _frontText;
    [SerializeField] private TMP_Text _topText;

    [SerializeField] private Vector3 _positionOffset;

    public byte Multiplier { get; private set; }
    
    public void Init(byte multiplier)
    {
        Multiplier = multiplier;
        _frontText.text = _topText.text = $"x{multiplier + 1}";
        transform.localPosition = multiplier * Offset;
    }

    public void SetColor(Color color) => _renderer.material.color = color;

    public Vector3 GetDestination() => transform.position + _positionOffset;

#if UNITY_EDITOR
    private void OnDrawGizmosSelected() =>
        GizmosUtilities.DrawOutlineDisc(GetDestination(), Vector3.up, .15f, new Color(1, .92f, .02f, .16f), Color.yellow);
#endif
}