﻿using MightyAttributes;
#if UNITY_EDITOR
using MightyAttributes.Utilities;
using UnityEditor;
#endif
using UnityEngine;

public class TurnTrackBehaviour : MonoBehaviour
{
    [SerializeField, LayerField] private int _playerLayer;
    [SerializeField] private Vector3 _turnAngles;
    [SerializeField, PositionHandle(true)] private Vector3 _turnDestinationOffset;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != _playerLayer) return;

        var position = transform.position;
        GameManager.Instance.PlayerBehaviour.TurnTowards(_turnAngles, position + _turnDestinationOffset);
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        var position = transform.position;
        GizmosUtilities.DrawOutlineDisc(position + _turnDestinationOffset, Vector3.up, .4f, new Color(1, .92f, .02f, .16f), Color.yellow);

        Handles.color = Color.yellow;
        Handles.ArrowHandleCap(0, position, Quaternion.Euler(_turnAngles), 1, EventType.Repaint);
    }
#endif
}