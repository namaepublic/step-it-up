﻿using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    [SerializeField] private Vector3 offset;

    private Transform m_transform;

    public void Init(Vector3 targetPosition)
    {
        m_transform = transform;
        
        SetPosition(targetPosition);
    }

    public void FixedUpdateCamera(Vector3 targetPosition) => SetPosition(targetPosition);

    private void SetPosition(Vector3 position) => m_transform.position = position + offset;
}
